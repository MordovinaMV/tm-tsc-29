package ru.tsc.mordovina.tm.api;

import org.jetbrains.annotations.NotNull;

public interface ISaltSetting {

    @NotNull String getPasswordSecret();

    @NotNull Integer getPasswordIteration();

}
